<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'slug' => ['nullable', 'string', Rule::unique('categories')->ignore($this->category->id)],
            '_lft' => ['nullable', 'integer', 'gt:0'],
            '_rgt' => ['nullable', 'integer', 'gt:0'],
            'parent_id' => ['nullable', 'integer', 'exists:categories,id'],
        ];
    }
}
