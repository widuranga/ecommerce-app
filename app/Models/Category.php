<?php

namespace App\Models;

use App\Models\Traits\HasSlug;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use HasSlug;
    use HasFactory;
    use NodeTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        '_lft',
        '_rgt',
        'parent_id',
    ];

//    public function products(): BelongsToMany
//    {
//        return $this->belongsToMany(Product::class);
//    }

//    /**
//     * The attributes that should be cast to native types.
//     *
//     * @var array
//     */
//    protected $casts = [
//        'id' => 'integer',
//        '_lft' => 'integer',
//        '_rgt' => 'integer',
//        'parent_id' => 'integer',
//    ];
//
//
    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Categories::class);
    }
}
