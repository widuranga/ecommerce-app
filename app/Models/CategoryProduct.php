<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class CategoryProduct extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'category_product';
    public $timestamps = false;
    protected $fillable = [
        'category_id',
        'product_id',
    ];

}
