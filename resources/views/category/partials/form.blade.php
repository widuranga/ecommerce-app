<div class="card-body">
    <div class="row">
        <div class="form-group col-md-4">
            {!! Form::label('name', 'Name*', ['for' => 'name']) !!}
            {!! Form::text('name', $name, ['autocomplete'=>'off','id'=>'name','class' => 'form-control','placeholder'=>'Enter Name']) !!}
            @if($errors->has('name'))
                <p class="help-block">
                    {{ $errors->first('name') }}
                </p>
            @endif
        </div>

        <div class="form-group col-md-4">
            {!! Form::label('slug', 'Slug URL', ['for' => 'name']) !!}
            {!! Form::text('slug', $slug, ['autocomplete'=>'off','id'=>'slug','class' => 'form-control','placeholder'=>'Enter slug']) !!}
            @if($errors->has('slug'))
                <p class="help-block">
                    {{ $errors->first('slug') }}
                </p>
            @endif
        </div>
        <div class="form-group col-md-4">
            {!! Form::label('parent_id', 'Select parent', ['for' => 'request']) !!}
            {!! Form::select('parent_id',$categories,$category,['class'=>'chosen form-control','id'=>'parent_id','data-placeholder'=>'Select parent category']) !!}
            @if($errors->has('parent_id'))
                <p class="help-block">
                    {{ $errors->first('parent_id') }}
                </p>
            @endif
        </div>
    </div>
</div>
