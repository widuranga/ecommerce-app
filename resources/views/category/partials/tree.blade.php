<li
    data-jstree='{"id":{{$category->id}},"parent":{{$category->parent_id}}}'
    data-url="{{route('category.edit',$category->id)}}"
    id="{{$category->id}}">{{$category->name}}
    @if(count($category->children)>0)
        <ul>
            @foreach($category->children as $category)
                @include('category.partials.tree', $category)
            @endforeach
        </ul>
    @endif
</li>
