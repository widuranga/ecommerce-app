@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Category - List</h4>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div id="test" class="demo">
                            <ul>
                                <li data-jstree='{ "opened" : true }'>Root node
                                    @if(count($categories)>0)
                                        <ul>
                                            @foreach($categories as $key=>$category)
                                                @include('category.partials.tree', $category)
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @endsection

            @section('script')
                <script>
                    $(document).ready(function () {
                        $('#test').on('select_node.jstree', function (e, data) {
                            window.open(data.node.data.url, '_blank');
                        }).on('move_node.jstree', function (e, data) {
                            console.info(data);
                        })
                            .jstree({
                                "core": {
                                    "animation": 0,
                                    "check_callback": true,
                                    "themes": {"stripes": true},
                                },
                                "types": {
                                    "#": {
                                        "max_children": 1,
                                        "max_depth": 4,
                                        "valid_children": ["root"]
                                    },
                                    "root": {
                                        "icon": "/static/3.3.11/assets/images/tree_icon.png",
                                        "valid_children": ["default"]
                                    },
                                    "default": {
                                        "valid_children": ["default", "file"]
                                    },
                                    "file": {
                                        "icon": "glyphicon glyphicon-file",
                                        "valid_children": []
                                    }
                                },
                                "plugins": [
                                    "contextmenu", "dnd", "search",
                                    "state", "types", "wholerow"
                                ]
                            });
                    });

                </script>
@endsection

