<div class="card-body">
    <div class="row">
        <div class="form-group col-md-3">
            {!! Form::label('name', 'Name*', ['for' => 'name']) !!}
            {!! Form::text('name', $name, ['autocomplete'=>'off','id'=>'name','class' => 'form-control','placeholder'=>'Enter Name']) !!}
            @if($errors->has('name'))
                <p class="help-block">
                    {{ $errors->first('name') }}
                </p>
            @endif
        </div>

        <div class="form-group col-md-3">
            {!! Form::label('slug', 'Slug URL', ['for' => 'name']) !!}
            {!! Form::text('slug', $slug, ['autocomplete'=>'off','id'=>'slug','class' => 'form-control','placeholder'=>'Enter slug']) !!}
            @if($errors->has('slug'))
                <p class="help-block">
                    {{ $errors->first('slug') }}
                </p>
            @endif
        </div>
        <div class="form-group col-md-3">
            {!! Form::label('description', 'Description', ['for' => 'request']) !!}
            {!! Form::text('description',$description,['class'=>'form-control','id'=>'description','data-placeholder'=>'Select parent category']) !!}
            @if($errors->has('description'))
                <p class="help-block">
                    {{ $errors->first('description') }}
                </p>
            @endif
        </div>
        <div class="form-group col-md-3">
            {!! Form::label('category_id', 'Select Category', ['for' => 'request']) !!}
            {!! Form::select('category_id',$categories,$category,['class'=>'chosen form-control','id'=>'parent_id','data-placeholder'=>'Select a category']) !!}
            @if($errors->has('category_id'))
                <p class="help-block">
                    {{ $errors->first('category_id') }}
                </p>
            @endif
        </div>
    </div>
    <div class="row" style="margin-top: 10px" id="images">
        <div class="form-group col-md-3">
            <div class="input-group control-group increment">
                <input type="file" name="image[]" class="form-control" multiple>
            </div>
            @if($errors->has('image_t'))
                <p class="help-block">
                    {{ $errors->first('image_t') }}
                </p>
            @endif
        </div>
    </div>
</div>
