@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Product - Create</h4>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Product Name</th>
                                <th scope="col">Product Slag</th>
                                <th scope="col" colspan="5" style="alignment: center" width="50%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->slug }}</td>
                                    <td><a href="{{route('product.edit',$product->id)}}" class="badge-primary">Edit</a></td>
                                    <td><a href="" class="badge-success">Manage images</a></td>
                                    <td><a href="" class="badge-success">Manage prices</a></td>
                                    <td><a href="" class="badge-danger">Remove</a></td>
                                    <td><a href="" class="badge-info">view</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')


@endsection

